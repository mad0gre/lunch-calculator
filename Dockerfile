FROM php:7.2.11-alpine

WORKDIR /work/studyportals/

VOLUME /work/studyportals/output/

COPY ./Calculator.php .

CMD php -f Calculator.php > output/output.txt