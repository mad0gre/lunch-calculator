<?php

$lines = [
    '40.00 Thijs Danny,Danny,Thijs,Stefan,Den',
    '45.00 Danny Danny,Thijs,Stefan,Den',
    '36.00 Stefan Danny,Thijs,Stefan',
    '40.00 Stefan Danny,Thijs,stefan,Den',
    '40.00 Danny Danny,Thijs,Stefan,Den',
    '12.00 Stefan Thijs,Stefan,Den',
    '44.00 Danny Danny,Thijs,Stefan,Den',
    '42.40 Den Danny,Stefan,Den,Den',
    '40.00 danny Danny,Thijs,Stefan,Den',
    '50.40 Thijs Danny,Thijs,Den',
    '48.00 Den Danny,thijs,Stefan,Den',
    '84.00 Thijs Thijs,Stefan,den'
];

$Calculator = new Calculator($lines);
$Calculator->printBill();

class Calculator
{

    private $bills = [];

    public function __construct($bills)
    {
        foreach ($bills as $bill_item) {
            $this->bills[] = new BillItem($bill_item);
        }
    }

    public function printBill()
    {
        $payout = $this->calculate();

        foreach ($payout as $debtor => $lines) {
            $debtor = ucfirst($debtor);

            foreach ($lines as $creditor => $amount) {
                $amount = number_format($amount, 2);
                $creditor = ucfirst($creditor);
                echo "$debtor pays $creditor $amount" . PHP_EOL;
            }
        }
    }

    private function calculate()
    {
        $payout = [];

        foreach ($this->bills as $bill) {
            $price = $bill->getPrice();
            $payer = $bill->getPaid_by();
            $attendees = $bill->getAtendees();

            $splitpay = $price / count($attendees);
            $splitpay = round($splitpay, 2, PHP_ROUND_HALF_EVEN);
            foreach ($attendees as $attendee) {
                if ($attendee === $payer) {
                    continue;
                }
                $payout[$attendee][$payer] += $splitpay;
            }
        }

        $payout = $this->flatPayout($payout);
        $payout = $this->clearPayout($payout);

        return $payout;
    }

    private function flatPayout($payout)
    {
        $flatPayout = $this->flatPayoutReciprocal($payout);
        $flatPayout = $this->flatPayoutTransitive($flatPayout);

        return $flatPayout;
    }

    private function flatPayoutReciprocal($payout)
    {
        $flatPayout = [];

        foreach ($payout as $debtor => $creditors) {
            foreach ($creditors as $creditor => $value) {
                if ($value > 0 && $payout[$creditor][$debtor] > 0) {
                    $diff = $value - $payout[$creditor][$debtor];
                    if ($diff < 0) {
                        $flatPayout[$creditor][$debtor] = 0 - $diff;
                        $flatPayout[$debtor][$creditor] = 0;
                    } else {
                        $flatPayout[$creditor][$debtor] = 0;
                        $flatPayout[$debtor][$creditor] = $diff;
                    }
                } else {
                    $flatPayout[$debtor][$creditor] = $value;
                }
            }
        }

        return $flatPayout;
    }

    private function flatPayoutTransitive($payout)
    {
        foreach ($payout as $debtor => $creditors) {
            foreach ($creditors as $c1 => $v1) {
                foreach ($creditors as $c2 => $v2) {
                    if ($c1 === $c2) {
                        continue;
                    } else {
                        $payout = $this->checkTransitivePayment($payout, $debtor, $c1, $c2);
                    }
                }
            }
        }

        return $payout;
    }

    private function checkTransitivePayment($payout, $p1, $p2, $p3)
    {
        if ($payout[$p1][$p3] > 0
            && $payout[$p2][$p3] > 0
            && $payout [$p1][$p2] > 0) {
            if ($payout[$p1][$p2] > $payout[$p2][$p3]) {
                $payout[$p1][$p3] += $payout[$p2][$p3];
                $payout[$p1][$p2] -= $payout[$p2][$p3];
                $payout[$p2][$p3] = 0;
            } else {
                $payout[$p1][$p3] += $payout[$p1][$p2];
                $payout[$p2][$p3] -= $payout[$p1][$p2];
                $payout[$p1][$p2] = 0;
            }
        }

        return $payout;
    }

    private function clearPayout($payout)
    {
        foreach ($payout as $debtor => $creditors) {
            foreach ($creditors as $creditor => $value) {
                if ($payout[$debtor][$creditor] === 0) {
                    unset($payout[$debtor][$creditor]);
                }
            }
            if ($payout[$debtor] === []) {
                unset($payout[$debtor]);
            }
        }
        return $payout;
    }
}

class BillItem
{
    private $price;
    private $paid_by;
    private $attendees = [];

    public function __construct($row)
    {
        $data = explode(' ', $row);
        $this->price = (float)$data[0];
        $this->paid_by = strtolower($data[1]);

        foreach (explode(',', $data[2]) as $debtor) {
            $this->attendees[] = strtolower($debtor);
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPaid_by()
    {
        return $this->paid_by;
    }

    public function getAtendees()
    {
        return $this->attendees;
    }
}

?>