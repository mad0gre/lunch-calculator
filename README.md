# lunch-calculator


### Requirements.
In order to build and run the project you will need Docker - Tested using version 18.09.0, but older versions might work as well.

### Run
The easiest way to run the project is by executing the Dockerfile contained therein:
* `docker build -t lunch-calculator .`
* `docker run -v /path/to/mount/point/:/work/studyportals/output/ lunch-calculator`.

After the container runs, a file called `output.txt` will be available at the mount point.

##### Docker volume
* `-v /path/to/mount/point:/work/studyportals/output/`: Volume. It bounds a path in the host machine to a path inside the container. The output file generated in the container will be available in the host machine.